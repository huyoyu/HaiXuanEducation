//
//  HYStudyViewController.m
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/18.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import "HYStudyViewController.h"

// 三个子视图控制器
#import "HYRequiredViewController.h"
#import "HYOptionViewController.h"
#import "HYDownloadViewController.h"

// UIBarButtonItem类别
#import "UIBarButtonItem+HYBarButtonItem.h"
// 标签按钮类别
#import "HYTitleButton.h"
// UIView类别
#import "UIView+HYViewCategory.h"

// 宏
// 导航栏高度
#define kHYNavBarH 64
// 标签栏高度
#define kHYTitleViewH 35

@interface HYStudyViewController ()<UIScrollViewDelegate>
/**
 *  存放所有的子控制器
 */
@property (nonatomic, weak) UIScrollView *scrollView;
/**
 *  标签栏视图
 */
@property (nonatomic, weak) UIView *titleView;
/**
 *  存放标签按钮
 */
@property (nonatomic, strong) NSMutableArray *titleBtnArr;
/**
 *  标签栏中标签按钮下的滑动条
 */
@property (nonatomic, weak) UIView *titleBottomView;
/**
 *  当前被点击的按钮
 */
@property (nonatomic, weak) HYTitleButton *selectedTitleButton;
@end

@implementation HYStudyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    self.navigationItem.title = @"学习";
    
    [self setupNav];
    [self setupChildVcs];
    [self setupScrollView];
    [self setupTitleView];
}

#pragma mark - private methods
/**
 *  自定义导航栏
 */
- (void)setupNav {
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithImage:@"mine-setting-iconN" highImage:nil target:self action:@selector(settingClick)];
}

// 添加三个子视图控制器
- (void)setupChildVcs {
    HYRequiredViewController *requiredVC = [[HYRequiredViewController alloc] init];
    requiredVC.title = @"必修";
    [self addChildViewController:requiredVC];
    
    HYOptionViewController *optionVC = [[HYOptionViewController alloc] init];
    optionVC.title = @"选修";
    [self addChildViewController:optionVC];
    
    HYDownloadViewController *downloadVC = [[HYDownloadViewController alloc] init];
    downloadVC.title = @"下载";
    [self addChildViewController:downloadVC];
}

- (void)setupScrollView {
    // 不要自动调增scrollView的contentInset
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.frame = self.view.bounds;
    //scrollView.backgroundColor = [UIColor yellowColor];
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(self.childViewControllers.count * self.view.width, 0);
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    // 调用代理方法,使默认显示第0个控制器
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

- (void)setupTitleView {
    // 标签栏整体
    UIView *titleView = [[UIView alloc] init];
    titleView.backgroundColor = [UIColor colorWithRed:0.7426 green:1.0 blue:0.995 alpha:1.0];
    titleView.frame = CGRectMake(0, kHYNavBarH, self.view.width, kHYTitleViewH);
    [self.view addSubview:titleView];
    self.titleView = titleView;
    
    // 标签栏内部的标签按钮
    NSUInteger count = self.childViewControllers.count;
    CGFloat titleButtonH = titleView.height;
    CGFloat titleButtonW = titleView.width/2/count;
    for (int i = 0; i < count; i ++) {
        // 创建标签按钮
        HYTitleButton *titleButton = [HYTitleButton buttonWithType:UIButtonTypeCustom];
        [titleButton addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:titleButton];
        [self.titleBtnArr addObject:titleButton];
        
        // 设置标签文字
        NSString *titleText = [self.childViewControllers[i] title];
        [titleButton setTitle:titleText forState:UIControlStateNormal];
        
        // 设置标签的frame
        titleButton.y = 0;
        titleButton.height = titleButtonH;
        titleButton.width = titleButtonW;
        titleButton.x = (self.view.width - 3*titleButtonW)/2.0f + i *titleButtonW;
    }
    // **** 标签按钮下的滚动条 ****
    UIView *titleBottomView = [[UIView alloc] init];
    titleBottomView.backgroundColor = [self.titleBtnArr.lastObject titleColorForState:UIControlStateSelected];
    titleBottomView.height = 2;
    titleBottomView.y = titleView.height - titleBottomView.height;
    [titleView addSubview:titleBottomView];
    self.titleBottomView = titleBottomView;
    
    // 使滚动条的frame与标签按钮的一致
    HYTitleButton *firstTitleButton = self.titleBtnArr.firstObject;
    [firstTitleButton.titleLabel sizeToFit];
    titleBottomView.width = firstTitleButton.titleLabel.width;
    titleBottomView.centerX = firstTitleButton.centerX;
    
    // 默认点击最前面的按钮
    [self titleClick:firstTitleButton];
}

#pragma mark - event response
// 导航栏工具按钮响应
- (void)settingClick {
    
}

- (void)titleClick:(HYTitleButton *)titleButton {
    // 当点击按钮时,将当前按钮取消选中状态,当前点击的按钮设为选中状态,并将点击的按钮设置为当前按钮
    self.selectedTitleButton.selected = NO;
    titleButton.selected = YES;
    self.selectedTitleButton = titleButton;
    
    // 滑动条的滑动动画延时
    [UIView animateWithDuration:0.25 animations:^{
        self.titleBottomView.width = titleButton.titleLabel.width;
        self.titleBottomView.centerX = titleButton.centerX;
    }];
    
    // 点击按钮的时候让scrollView滚动到对应的位置
    CGPoint offset = self.scrollView.contentOffset;
    offset.x = self.view.width * [self.titleBtnArr indexOfObject:titleButton];
    [self.scrollView setContentOffset:offset animated:YES];
}

#pragma mark - UIScrollViewDegate
// 当scrollView滚动动画结束后,将子视图控制器添加到scrollView上
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    // 取出对应的控制器
    int index = scrollView.contentOffset.x / scrollView.width;
    UIViewController *currentVC = self.childViewControllers[index];
    
    // 如果控制器的View已经被创建过,就直接返回
    if (currentVC.isViewLoaded) {
        return;
    }
    
    // 添加子控制器的view到scrollVIew上
    currentVC.view.frame = scrollView.bounds;
    [scrollView addSubview:currentVC.view];
}

// 手动滚动scrollView的时候,当减速完成后,标签按钮也滚动到对应位置
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self scrollViewDidEndScrollingAnimation:scrollView];
    
    // 点击按钮,调用按钮点击方法
    int index = scrollView.contentOffset.x / scrollView.width;
    [self titleClick:self.titleBtnArr[index]];
}

#pragma mark - getters&setters 
- (NSMutableArray *)titleBtnArr {
    if (!_titleBtnArr) {
        _titleBtnArr = [NSMutableArray array];
    }
    return _titleBtnArr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/










@end
