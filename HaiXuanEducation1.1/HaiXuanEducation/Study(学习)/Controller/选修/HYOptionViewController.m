//
//  HYOptionViewController.m
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/19.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import "HYOptionViewController.h"


@interface HYOptionViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation HYOptionViewController

- (void)viewDidLoad {
    [self.view addSubview:self.myTab];
    [self setupRefresh];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    self.myTab.mj_footer.hidden = NO;
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HYStudyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HYStudyCell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //[cell.testTimeLab removeFromSuperview];
    return cell;
}

@end
