//
//  HYCommonViewController.h
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/19.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HYStudyCell.h"

#import "MJRefreshGifHeader.h"
#import "MJRefreshAutoGifFooter.h"

@interface HYCommonViewController : UIViewController

@property (nonatomic, strong) UITableView *myTab;

- (void)setupRefresh;

@end
