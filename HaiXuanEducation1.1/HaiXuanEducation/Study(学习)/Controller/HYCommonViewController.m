//
//  HYCommonViewController.m
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/19.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import "HYCommonViewController.h"

#import "UITableView+Regist.h"

#import <AFNetworking.h>

// 导航栏高度
#define kHYNavBarH 64
// 标签栏高度
#define kHYTitleViewH 35

#define kHYTabBarH 49

@interface HYCommonViewController ()<UITableViewDelegate,UITableViewDataSource> {
    
}


/** 所有状态对应的动画图片 */
@property (strong, nonatomic) NSMutableDictionary *stateImages;
/** 所有状态对应的动画时间 */
@property (strong, nonatomic) NSMutableDictionary *stateDurations;

@end

@implementation HYCommonViewController
static int count = 2;

- (void)viewDidLoad {
    
    [self.view addSubview:self.myTab];
    [self setupRefresh];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    self.myTab.mj_footer.hidden = NO;
    count += 2;
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HYStudyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HYStudyCell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //cell.headImageV.image = [UIImage imageNamed:@"1.jpg"];
    //cell.textLabel.text = @"1234";
    return cell;
}

#pragma mark - UITableViewDelegate
#warning 此处单元格高度计算根据模型解析传过来的内容进行分析计算各个模块的高度相加返回,做法为新建一个NSObject类,将当前cell中的模型传递过去,并利用模型类进行解析获得模型类的内容进行计算高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105.5;
    //HYStudyCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //return cell.heightCell;
}

#pragma mark - event response
- (void)loadNewInformation {
    // 下拉刷新
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.myTab.mj_header endRefreshing];
    });
    [self.myTab reloadData];
}


- (void)loadMoreInformation {
    // 上拉加载
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.myTab.mj_footer endRefreshing];
    });
    self.myTab.mj_footer.hidden = YES;
    [self.myTab reloadData];
}

#pragma mark - private methods
// 刷新
- (void)setupRefresh {
    // 设置普通状态的动画图片
    NSMutableArray *idleImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<=60; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_anim__000%zd", i]];
        [idleImages addObject:image];
    }
    // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
    NSMutableArray *refreshingImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<=3; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_loading_0%zd", i]];
        [refreshingImages addObject:image];
    }
    
    // 下拉刷新
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewInformation)];
    [header setImages:idleImages forState:MJRefreshStateIdle];
    [header setImages:refreshingImages forState:MJRefreshStatePulling];
    [header setImages:refreshingImages forState:MJRefreshStateRefreshing];
    self.myTab.mj_header = header;
    
    //设置自定义文字，因为默认是英文的
    [header setTitle:@"下拉刷新"forState:MJRefreshStateIdle];
    [header setTitle:@"松开加载更多"forState:MJRefreshStatePulling];
    [header setTitle:@"正在刷新中"forState:MJRefreshStateRefreshing];
    header.stateLabel.font = [UIFont systemFontOfSize:15];
    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:14];
    header.stateLabel.textColor = [UIColor redColor];
    header.lastUpdatedTimeLabel.textColor = [UIColor blueColor];
    
    // 上拉加载
    MJRefreshAutoGifFooter *footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreInformation)];
    [footer setImages:refreshingImages forState:MJRefreshStateRefreshing];
    self.myTab.mj_footer = footer;
    
    [footer setTitle:@"上拉刷新"forState:MJRefreshStateIdle];
    [footer setTitle:@"松开加载更多"forState:MJRefreshStatePulling];
    [footer setTitle:@"正在刷新中"forState:MJRefreshStateRefreshing];
    footer.stateLabel.font = [UIFont systemFontOfSize:15];
    //footer.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:14];
    footer.stateLabel.textColor = [UIColor redColor];
    //footer.lastUpdatedTimeLabel.textColor = [UIColor blueColor];
}

#pragma mark - getters&setters
- (UITableView *)myTab {
    if (!_myTab) {
        _myTab = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _myTab.contentInset = UIEdgeInsetsMake(kHYNavBarH + kHYTitleViewH, 0, kHYTabBarH, 0);
        _myTab.scrollIndicatorInsets = _myTab.contentInset;
        // 分割线
        //_myTab.separatorStyle = UITableViewCellAccessoryNone;
        [_myTab zy_registClassCell:[HYStudyCell class]];
        _myTab.delegate = self;
        _myTab.dataSource = self;
    }
    return _myTab;
}


@end
