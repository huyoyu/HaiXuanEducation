//
//  HYStudyCell.h
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/20.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HEX.h"

@interface HYStudyCell : UITableViewCell

@property (nonatomic, strong) UIImageView *headImageV;
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UILabel *progressLab;
@property (nonatomic, strong) UILabel *percentLab;
@property (nonatomic, strong) UIProgressView *progressV;
@property (nonatomic, strong) UILabel *testTimeLab;
@property (nonatomic, strong) UILabel *testScoreLab;

@property (nonatomic, assign) CGFloat heightCell;

@end
