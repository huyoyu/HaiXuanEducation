//
//  HYStudyCell.m
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/20.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import "HYStudyCell.h"
#import <Masonry.h>

#define kHYCalculate(number) number/640.0*375
#define kHYPadding 12
#define kHYTextFont 16

@implementation HYStudyCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
        [self setupDetailCell];
    }
    return self;
}

- (CGFloat)heightCell {
    return 300;
}

- (void)setupDetailCell {
    [self addSubview:self.headImageV];
    [self addSubview:self.titleLab];
    [self addSubview:self.progressLab];
    [self addSubview:self.progressV];
    [self addSubview:self.percentLab];
    [self addSubview:self.testTimeLab];
    [self addSubview:self.testScoreLab];
    
    [self.headImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.offset(kHYCalculate(kHYPadding));
        make.width.equalTo(@(kHYCalculate(148)));
        make.height.equalTo(@(kHYCalculate(120)));
    }];
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headImageV.mas_right).offset(kHYCalculate(kHYPadding));
        make.top.offset(kHYCalculate(kHYPadding));
        //make.width.equalTo(@(kHYCalculate(148)));
        make.height.equalTo(@(kHYCalculate(30)));
    }];
    
    [self.progressLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headImageV.mas_right).offset(kHYCalculate(kHYPadding));
        make.top.equalTo(self.titleLab.mas_bottom).offset(kHYCalculate(kHYPadding));
        //make.width.equalTo(@(kHYCalculate(148)));
        make.height.equalTo(@(kHYCalculate(30)));
    }];
    
    [self.progressV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.progressLab.mas_right).offset(kHYCalculate(kHYPadding));
        make.centerY.equalTo(self.progressLab.mas_centerY);
        make.width.equalTo(@(kHYCalculate(120)));
        make.height.equalTo(@(kHYCalculate(8)));
    }];
    
    [self.percentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.progressV.mas_right).offset(kHYCalculate(30));
        make.centerY.equalTo(self.progressLab.mas_centerY);
        //make.width.equalTo(@(kHYCalculate(12)));
        make.height.equalTo(@(kHYCalculate(30)));
    }];
    
    [self.testTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headImageV.mas_right).offset(kHYCalculate(kHYPadding));
        make.top.equalTo(self.progressLab.mas_bottom).offset(kHYCalculate(kHYPadding));
        //make.width.equalTo(@(kHYCalculate(120)));
        make.height.equalTo(@(kHYCalculate(30)));
    }];
    
    [self.testScoreLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headImageV.mas_right).offset(kHYCalculate(kHYPadding));
        make.top.equalTo(self.testTimeLab.mas_bottom).offset(kHYCalculate(kHYPadding));
        //make.width.equalTo(@(kHYCalculate(120)));
        make.height.equalTo(@(kHYCalculate(30)));
    }];

}

#pragma mark - 懒加载
- (UIImageView *)headImageV {
    if (!_headImageV) {
        _headImageV = [[UIImageView alloc] init];
        _headImageV.frame = CGRectMake(0, 0, 100, 100);
#warning 根据后期资源加载
        _headImageV.image = [UIImage imageNamed:@"1.jpg"];
        _headImageV.backgroundColor = [UIColor greenColor];
    }
    return _headImageV;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"粉丝在前 营销在后";
        _titleLab.layer.borderColor = [UIColor colorWithRed:0.804 green:0.8458 blue:0.4546 alpha:1.0].CGColor;//边框颜色,要为CGColor
        _titleLab.layer.borderWidth = 1;
        //_titleLab.numberOfLines = 0;
        _titleLab.textColor = [UIColor colorWithHexString:@"#545454"];
        [_titleLab sizeToFit];
        //_titleLab.font = [UIFont systemFontOfSize:kHYTextFont];
        _titleLab.adjustsFontSizeToFitWidth = YES;
    }
    return _titleLab;
}

- (UILabel *)progressLab {
    if (!_progressLab) {
        _progressLab = [[UILabel alloc] init];
        _progressLab.text = @"学习进度:";
        //_progressLab.numberOfLines = 0;
        _progressLab.textColor = [UIColor colorWithHexString:@"#b8b8b8"];
        [_progressLab sizeToFit];
        //_progressLab.font = [UIFont systemFontOfSize:kHYTextFont];
        _progressLab.adjustsFontSizeToFitWidth = YES;
    }
    return _progressLab;
}

- (UIProgressView *)progressV {
    if (!_progressV) {
        _progressV = [[UIProgressView alloc] init];
        _progressV.progressViewStyle = UIProgressViewStyleDefault;
        //_progressV.bounds = CGRectMake(0, 0, 60, 10);
        // 轨道颜色
        _progressV.trackTintColor = [UIColor colorWithHexString:@"f0f2f5"];
        // 进度条颜色
        _progressV.progressTintColor = [UIColor colorWithHexString:@"#70c284"];
        // 进度条的进度0-1
        _progressV.progress = 0.7;
    }
    return _progressV;
}

- (UILabel *)percentLab {
    if (!_percentLab) {
        _percentLab = [[UILabel alloc] init];
        _percentLab.text = @"70%";
        //_percentLab.numberOfLines = 0;
        _percentLab.textColor = [UIColor colorWithHexString:@"#b8b8b8"];
        [_percentLab sizeToFit];
        //_percentLab.font = [UIFont systemFontOfSize:kHYTextFont];
        _percentLab.adjustsFontSizeToFitWidth = YES;
    }
    return _percentLab;
}

- (UILabel *)testTimeLab {
    if (!_testTimeLab) {
        _testTimeLab = [[UILabel alloc] init];
        _testTimeLab.text = @"考核时间:  2016-7-20";
        //_testTimeLab.numberOfLines = 0;
        _testTimeLab.textColor = [UIColor colorWithHexString:@"#b8b8b8"];
        [_testTimeLab sizeToFit];
        //_testTimeLab.font = [UIFont systemFontOfSize:kHYTextFont];
        _testTimeLab.adjustsFontSizeToFitWidth = YES;
    }
    return _testTimeLab;
}

- (UILabel *)testScoreLab {
    if (!_testScoreLab) {
        _testScoreLab = [[UILabel alloc] init];
        _testScoreLab.text = @"考核成绩: 未考试";
        //_testScoreLab.numberOfLines = 0;
        _testScoreLab.textColor = [UIColor colorWithHexString:@"#b8b8b8"];
        [_testScoreLab sizeToFit];
        //_testScoreLab.font = [UIFont systemFontOfSize:kHYTextFont];
        _testScoreLab.adjustsFontSizeToFitWidth = YES;
    }
    return _testScoreLab;
}











@end
