//
//  HYNotesViewController.m
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/18.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import "HYNotesViewController.h"

@interface HYNotesViewController ()

@end

@implementation HYNotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    self.navigationItem.title = @"笔记";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
