//
//  AppDelegate.h
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/18.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

