//
//  UIBarButtonItem+HYBarButtonItem.h
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/19.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (HYBarButtonItem)

+ (instancetype)itemWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action;

@end
