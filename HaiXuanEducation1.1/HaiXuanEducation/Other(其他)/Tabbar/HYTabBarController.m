//
//  HYTabBarController.m
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/18.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import "HYTabBarController.h"

// 四个子视图控制器
#import "HYStudyViewController.h"
#import "HYTestViewController.h"
#import "HYNotesViewController.h"
#import "HYConclusionViewController.h"

@interface HYTabBarController ()

@end

@implementation HYTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置item属性
    [self setupItem];
    
    // 添加子视图控制器
    [self setupChildVcs];
    
}

/**
 *  添加子视图控制器
 */
- (void)setupChildVcs {
    [self setupchildVc:[[HYStudyViewController alloc] init] title:@"学习" image:@"tabBar_essence_icon" selectedImage:@"tabBar_essence_click_icon"];
    [self setupchildVc:[[HYTestViewController alloc] init] title:@"考试" image:@"tabBar_new_icon" selectedImage:@"tabBar_new_click_icon"];
    [self setupchildVc:[[HYNotesViewController alloc] init] title:@"笔记" image:@"tabBar_friendTrends_icon" selectedImage:@"tabBar_friendTrends_click_icon"];
    [self setupchildVc:[[HYConclusionViewController alloc] init] title:@"总结" image:@"tabBar_me_icon" selectedImage:@"tabBar_me_click_icon"];
    
    self.selectedIndex = 0;
}

/**
 *  添加一个子视图控制器
 *
 *  @param vc            所添加的子视图控制器
 *  @param title         子视图控制器的标题
 *  @param image         子视图控制器未选中的图片
 *  @param selectedImage 子视图控制器选中的图片
 */
- (void)setupchildVc:(UIViewController *)vc title:(NSString *)title
               image:(NSString *)image selectedImage:(NSString *)selectedImage {
    // 添加一个导航控制器
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    [self addChildViewController:nav];
    
    
    // 设置子控制器的tabBarItem
    nav.tabBarItem.title = title;
    nav.tabBarItem.image = [[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nav.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

}

/**
 *  设置item属性
 */
- (void)setupItem {
    // UIControlStateNormal状态下的文字属性
    NSMutableDictionary *normalDic = [NSMutableDictionary dictionary];
    // 文字颜色
    normalDic[NSForegroundColorAttributeName] = [UIColor grayColor];
    // 文字大小
    normalDic[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    
    // UIControlStateSelected状态下的文字属性
    NSMutableDictionary *selectedDic = [NSMutableDictionary dictionary];
    // 文字颜色
    selectedDic[NSForegroundColorAttributeName] = [UIColor blackColor];
    
    // 同意给所有的UITabBarItem设置文字属性
    // 只有后面带有UI_APPEARANCE_SELECTOR的属性或方法,才可以通过apperance对象来统一设置
    UITabBarItem *item = [UITabBarItem appearance];
    [item setTitleTextAttributes:normalDic forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectedDic forState:UIControlStateSelected];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
