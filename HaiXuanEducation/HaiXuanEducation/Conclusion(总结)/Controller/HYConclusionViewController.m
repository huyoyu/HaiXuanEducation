//
//  HYConclusionViewController.m
//  HaiXuanEducation
//
//  Created by haixuan on 16/7/18.
//  Copyright © 2016年 华惠友. All rights reserved.
//

#import "HYConclusionViewController.h"

@interface HYConclusionViewController ()

@end

@implementation HYConclusionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor greenColor];
    self.navigationItem.title = @"总结";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
